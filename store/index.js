import Vue from 'vue'
import { getByBrand, getByPrice, getByOffers, getAllProducts, getByCategory } from '../helpers/product-filters.js'

export const store = () => ({
  state: {
    products: [],
    cart: [],
    sortDir: 'asc',
    sortKey: null,
    category: null,
    results: 0,
    activeBrand: null,
    filteredProducts: []
  }
})
  
export const mutations = {
  INITIALIZE_CART: (state) => {
    if (localStorage.getItem('cart')) {
      // state.cart = JSON.parse(localStorage.getItem('cart'))
      Vue.set(state,'cart', JSON.parse(localStorage.getItem('cart')))
    } else {
      state.cart = []
    }
  },
  // 2. after the action 'fetchProducts' set the data in state
  SAVE_PRODUCT_DATA: (state, products) => {
    // state.products = products
    Vue.set(state, 'products', products);
  },

  SET_FILTERED_DATA: (state, products) => {
    // state.filteredProducts = products || state.products
    // state.results = products.length
    Vue.set(state, 'filteredProducts', products)
    Vue.set(state, 'results', products.length)
  },

  ADD_TO_CART: (state, payload) => {
    let itemfound = state.cart.find(el => el.ProductID === payload.ProductID);

    if (itemfound) {
      itemfound.quantity = payload.quantity
      itemfound.inCart = payload.inCart
    } else {
      state.cart.push(payload)
    }
    // state.cartCount += payload.quantity
    window.localStorage.setItem('cart', JSON.stringify(state.cart));
  },

  REMOVE_FROM_CART: (state, payload) => {
    const remainingCart = state.cart.filter(prod => prod.ProductID !== payload.ProductID)
    // state.cart = remainingCart
    Vue.set(state, 'cart', remainingCart)
    window.localStorage.setItem('cart', JSON.stringify(state.cart));
  },

  SET_ACTIVE_BRAND: (state, brand) => {
    Vue.set(state, 'activeBrand', brand)
  },

  SAVE_PRODUCT_LS: state => {
    console.log('saving products in LS')
    window.localStorage.setItem('products', JSON.stringify(state.products))
  },

  SAVE_SORT_DIR: (state, payload) => {
    Vue.set(state, 'sortDir', payload.sortDir)
  },
  SAVE_SORT_KEY: (state, payload) => {
    Vue.set(state, 'sortKey', payload.sortKey)
  },
  SET_ACTIVE_CATEGORY: (state, payload) => {
    Vue.set(state, 'category', payload.category)
  }
}

export const getters = {
  getProducts: state => state.products,
  getCart: state => {
    console.log('STORE - cart', state.cart)
    return state.cart
  },
  getFilteredProducts: state => state.filteredProducts,
  getTotalProductsCount: state => {
    console.log('STORE - cart count', state.cart)
    if (!state.cart.length) return 0;
    return state.cart.reduce((ac, next) => ac + next.quantity, 0);
  },
  filterByBrand: state => {
    return getByBrand(state.products, state.activeBrand)
  },
  filterByCategory: state => {
    return getByCategory(state.products, state.category)
  },
  filterByKey: state => {
    // todo clean up
    if(state.sortKey === 'ProductOffers') {
      return getByOffers(state.products)  
    }
    if(state.sortKey === 'Price') {
      return getByPrice(state.products, state.sortDir)
    }
    if (state.sortKey === 'Reset') {
      return getAllProducts(state.products)
    }
  }
}

export const actions = {
  // 1. fetch data
  // 2. do a mutations so we can put the fetched products in state
  // 3. call setProducts() (method) in 'index.vue' for setting global state to local state in comp
  async fetchProducts({ commit }, { self }) {          
    const products = await fetch(`${process.env.API_URL}?api_key=${process.env.API_KEY}`)
      .then(res => res.json())
      .catch((error) => {
        console.log('ERROR -->', error.statusText)
      });

    // set products in store
    commit('SAVE_PRODUCT_DATA', products);

    // save to localstorage
    commit('SAVE_PRODUCT_LS', products); 
    
    self.setProducts()
  },

  // nullCheck (key) {
  //   return key === null ? 1 : -1
  // },

  // handleSortingByKey ({ commit }, payload) {
  //   const productsCopy = [...this.state.products];

  //   const key = payload.key
  //   const sortDir = payload?.sort

  //   let filteredList = []

  //   switch (key.toLowerCase()) {
  //     case 'productoffers':
  //       filteredList = productsCopy.filter(product => product[key].length > 0)
  //     break;

  //     case 'category':
  //       const category = payload.category
  //       const list = []

  //       if (category) {
  //         productsCopy.map((product) => {
  //           product.WebSubGroups.some(cat => {
  //            if (cat.Description.toLowerCase() === category.toLowerCase()) {
  //             list.push(product)
  //            }
  //           })
  //         });
  //       }
  //       filteredList = list
  //     break;

  //     case 'price': 
  //       filteredList = productsCopy.sort((a, b) => {
  //         let priceA = a.ProductPrices[0]?.Price
  //         let priceB = b.ProductPrices[0]?.Price

  //         priceA === null ? 1 : -1

  //         return sortDir === 'asc'
  //           ? priceA - priceB
  //           : priceB - priceA;
  //       });
  //     break;

  //     case 'brand':
  //       const brandToFilter = payload?.brand
  //       filteredList = productsCopy.filter((product) => {
  //         return product[key] === brandToFilter
  //       })
        
  //     break;

  //     case 'reset':
  //       filteredList = productsCopy
  //     break;

  //   default:
  //     console.log('Nothing filtered :(')
  //     break;
  //   }
  //   console.log('STORE - Filtered Products', filteredList)
  //   commit('SET_FILTERED_DATA', filteredList)
  // }
}