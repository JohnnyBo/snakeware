export const filters = {
  productOverview: [
    {
      id: 1,
      sortKey: 'ProductOffers',
      name: "Aanbieding"
    },
    {
      id: 2,
      sortKey: 'Price',
      name: "Prijs"
    },
    {
      id: 3,
      sortKey: 'Reset',
      name: "Reset"
    }
  ]
}

export function getByBrand (list, brand) {
  if (!brand) return list

  return list.filter((product) => product.Brand === brand)
}

export function getByPrice (list, sortDir) {
  const priceList = [...list].sort((a, b) => {
      let priceA = a.ProductPrices[0]?.Price
      let priceB = b.ProductPrices[0]?.Price

      priceA === null ? 1 : -1

      return sortDir === 'asc'
        ? priceA - priceB
        : priceB - priceA;
    });

    return priceList
}

export function getByOffers (list) {
  return list.filter(product => product.ProductOffers.length > 0)
}

export function getAllProducts (list) {
  return list
}

export function getByCategory (list, category) {
  if (!category) return list

  const items = []

  list.forEach((product) => {
    product.WebSubGroups.some(cat => {
      if (cat.Description.toLowerCase() === category.toLowerCase()) {
        items.push(product)
      }
    })
  });
  console.log(items)
  return items
}