const helpers = {
  onLoadImg () {
    return true
  },

  getByCategory(list, category) {
    if (!category) return list
    return list.filter(item => item.category == category)
  },

  getUniqueList (products, filter) {
    if (!products) return

    let list = []
    let uniqueList = []
      
    switch (filter) {
      case 'category':
        products.forEach(product => product.WebSubGroups.map(
          cat => list.push(cat.Description)
        ))
      break;

      case 'brands':
        products.forEach(product => list.push(product.Brand))
      break;

      default:
        uniqueList = products
      break;
    }
      
      // filter duplicated and sort by name
      uniqueList = [...new Set(list)].filter(Boolean).sort();

      return uniqueList
  },

  // depricated -- use product-filter.js
  // nullCheck (key) {
  //   return key === null ? 1 : -1
  // },

  // handleSortingByKey (products, payload) {
  //   const productsCopy = [...products]

  //   const key = payload.key
  //   const sortDir = payload?.sort

  //   let filteredList = []

  //   switch (key.toLowerCase()) {
  //     case 'productoffers':
  //       filteredList = productsCopy.filter(product => product[key].length > 0)
  //     break;

  //     case 'category':
  //       const category = payload.category
  //       const list = []

  //       if (category) {
  //         productsCopy.map((product) => {
  //           product.WebSubGroups.some(cat => {
  //            if (cat.Description.toLowerCase() === category.toLowerCase()) {
  //             list.push(product)
  //            }
  //           })
  //         });
  //       }
  //       filteredList = list
  //     break;

  //     case 'price': 
  //       filteredList = productsCopy.sort((a, b) => {
  //         let priceA = a.ProductPrices[0]?.Price
  //         let priceB = b.ProductPrices[0]?.Price

  //         priceA === null ? 1 : -1

  //         return sortDir === 'asc'
  //           ? priceA - priceB
  //           : priceB - priceA;
  //       });
  //     break;

  //     case 'brand':
  //       const brandToFilter = payload?.brand
  //       filteredList = productsCopy.filter((product) => {
  //         return product[key] === brandToFilter
  //       })
        
  //     break;

  //     case 'reset':
  //       filteredList = productsCopy
  //     break;

  //   default:
  //     console.log('Nothing filtered :(')
  //     break;
  //   }
  //   console.log('STORE - Filtered Products', filteredList)
  // }

}

export default helpers
